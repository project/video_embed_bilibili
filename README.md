 # CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers


 # INTRODUCTION
BiliBili is the chinese YouTube alternative.The Video Embed BiliBili modules
provides functionality to add/create a new media entity in the media library.
The module extends Video Embed field and his sub-module Video Embed Media


 # REQUIREMENTS
This module requires no modules outside of Drupal core.

INSTALLATION
Those are the following modules which are required :
- Video Embed Media
- Video Embed Field

Visit https://www.drupal.org/project/video_embed_field for further
information.

 # INSTALLATION
Install as you would normally install a contributed Drupal module.


 # CONFIGURATION
The Video Embed field Bilibili module allows you to render BiliBili
videos on your website. BiliBili is the Youtube alternative in China.
The module is based on the video embed field module and on
its sub-module video embed media meaning that the Bilibili videos
are created as media entities which means that you can take
advantage of the media library as well.

How to setup:
 1. Download and install the module
 2. Create a new media type and select “Bilibili” as media source.
 3. Create a new field of type “Media” and select the “Bilibili” media type.
 4. You can adjust the display settings so the video can be
rendered as an iframe.
