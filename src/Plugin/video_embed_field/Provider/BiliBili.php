<?php

namespace Drupal\video_embed_bilibili\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A BiliBili provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "BiliBili",
 *   title = @Translation("BiliBili")
 * )
 */
class BiliBili extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $id = $this->getVideoId();
    $embed_code = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'BiliBili',
      '#url' => "https://player.bilibili.com/player.html?bvid=$id&aid=$id",
      '#query' => [
        'autoplay' => $autoplay,
        'rel' => '0',
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
    return $embed_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $id = $this->getVideoId();
    $url = "https://api.bilibili.com/x/web-interface/view?bvid=$id";
    $response = json_decode(file_get_contents($url));
    $backup = $url;
    try {
      $image = $response->data->pic;
      $high_resolution = $image;
      $this->httpClient->head($high_resolution);
      return $high_resolution;
    }
    catch (\Exception $e) {
      \Drupal::logger('video_embed_bilibili')->error($e);
      return $backup;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match('@https:\/\/www\.bilibili\.com\/video\/((?<id>[^/]+)\/?)@', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

}
